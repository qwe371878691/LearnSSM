package com.supergo.tcc.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by on 2019/12/17.
 */
@FeignClient("tcc-b")
public interface ServiceBClient {

    @GetMapping("/rpc")
    String rpc(@RequestParam("value") String value);

}
