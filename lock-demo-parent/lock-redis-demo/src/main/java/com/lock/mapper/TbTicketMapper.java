package com.lock.mapper;

import com.lock.pojo.TbTicket;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created by on 2019/12/12.
 */
@org.apache.ibatis.annotations.Mapper
public interface TbTicketMapper extends Mapper<TbTicket> {
}
