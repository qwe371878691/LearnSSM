package src.dto;

import java.time.LocalDate;
import java.util.Date;

public class Book {
    private int id;

    private String name;

    private Double price;

    private LocalDate pulishDate;

    private String type;

    public Book() {
    }

    public Book(int id, String name, Double price, String type,LocalDate pulishDate ) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.pulishDate = pulishDate;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalDate getPulishDate() {
        return pulishDate;
    }

    public void setPulishDate(LocalDate pulishDate) {
        this.pulishDate = pulishDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
